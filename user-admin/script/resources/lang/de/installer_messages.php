<?php

return array (
  'title' => 'Laravel Installer',
  'next' => 'Nächster Schritt',
  'finish' => 'Installieren',
  'welcome' => 
  array (
    'title' => 'Willkommen zum Installer',
    'message' => 'Willkommen zum Laravel Installationsassistent.',
  ),
  'requirements' => 
  array (
    'title' => 'Vorraussetzungen',
  ),
  'permissions' => 
  array (
    'title' => 'Berechtigungen',
  ),
  'environment' => 
  array (
    'title' => 'Umgebungsvariablen',
    'save' => 'Speicher .env',
    'success' => 'Ihre .env Konfiguration wurde gespeichert.',
    'errors' => 'Ihre .env Konfiguration konnte nicht gespeichert werden, Bitte erstellen Sie diese Manuell.',
  ),
  'final' => 
  array (
    'title' => 'Fertig!',
    'finished' => 'Die Anwendung wurde erfolgreich Installiert.',
    'exit' => 'Hier Klicken zum Beenden',
  ),
  'checkPermissionAgain' => 'Erneut prüfen',
  'install' => 'Installieren',
  'updater' => 
  array (
    'final' => 
    array (
      'exit' => 'Klicke hier zum Beenden',
      'finished' => 'Die Datenbank der Anwendung wurde erfolgreich aktualisiert.',
      'title' => 'Beendet',
    ),
    'overview' => 
    array (
      'message' => 'Es gibt 1 Update.| Es gibt :number Updates',
      'title' => 'Übersicht',
    ),
    'title' => 'Laravel Updater',
    'welcome' => 
    array (
      'message' => 'Willkommen zum Update Wizard',
      'title' => 'Willkommen zum Updater',
    ),
  ),
);
