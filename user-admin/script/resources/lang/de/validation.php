<?php

return array (
  'accepted' => 'Das :attribute muss akzeptiert werden.',
  'active_url' => 'Das :attribute ist keine gültige URL.',
  'after' => 'Das :attribute muss ein Datum nach :date sein.',
  'alpha' => 'Das :attribute darf nur Buchstaben enthalten.',
  'alpha_dash' => 'Das :attribute darf nur Buchstaben, Nummern und Bindestriche beinhalten.',
  'alpha_num' => 'Das :attribute darf nur Buchstaben und Nummern beinhalten.',
  'array' => 'Das :attribute muss ein Array sein.',
  'before' => 'Das :attribute muss ein Datum vor :date sein.',
  'between' => 
  array (
    'array' => 'Das :attribute muss zwischen :min und :max sein.',
    'file' => 'Das :attribute muss zwischen :min und :max Kilobytes groß sein.',
    'numeric' => 'Das :attribute muss zwischen :min und :max sein.',
    'string' => 'Das :attribute darf nur :min bis :max Zeichen enthalten.',
  ),
  'url' => 'Das :attribute Format ist nicht valide.',
  'unique' => 'Das :attribute wurde schon gewählt.',
  'boolean' => 'Das :attribute Feld muss true oder false sein.',
  'confirmed' => 'Die :attribute Bestätigung passt nicht.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'benutzerdefinierte Nachricht',
    ),
  ),
  'date_format' => 'Das :attribute passt nicht zum :format Format.',
  'date' => 'Das :attribute ist kein valides Datum.',
  'different' => ':attribute und :other muss unterschiedlich sein.',
  'digits' => 'Das :attribute muss :digits Ziffern lang sein.',
  'digits_between' => 'Das :attribute muss zwischen :min und :max Ziffern beinhalten.',
  'dimensions' => 'Das :attribute hat eine invalide Bildgröße.',
  'distinct' => 'Das :attribute hat einen doppelten Eintrag.',
  'email' => 'Das :attribute muss eine valide Email Adresse sein.',
  'exists' => 'Das ausgewählte :attribute ist invalide.',
  'filled' => 'Das :attribute Feld ist erforderlich.',
  'image' => 'Das :attribute muss ein Bild sein.',
  'in' => 'Das ausgewählte :attribute ist invalide.',
  'in_array' => 'Das :attribute Feld existiert nicht in :other.',
  'integer' => 'Das :attribute muss ein Integer sein.',
  'ip' => 'Das :attribute muss eine valide IP Adresse sein.',
  'json' => 'Das :attribute muss ein valider JSON String sein.',
  'max' => 
  array (
    'array' => 'Das :attribute darf nicht mehr als :max Elemente beinhalten.',
    'file' => 'Das :attribute darf nicht größer als :max Kilobytes sein.',
    'numeric' => 'Das :attribute darf nicht größer als :max sein.',
    'string' => 'Das :attribute darf nicht mehr als :max Zeichen beinhalten.',
  ),
  'mimes' => 'Das :attribute muss eine Datei des Typs: :values sein.',
  'min' => 
  array (
    'array' => 'Das :attribute muss mindestens :min Elemente beinhalten.',
    'file' => 'Das :attribute muss mindestens :min Kilobytes groß sein.',
    'numeric' => 'Das :attribute muss mindestens :min sein.',
    'string' => 'Das :attribute muss mindesten :min Zeichen beinhalten.',
  ),
  'not_custom_fields' => 'Dieses Feld existiert bereits in der Benutzer Tabelle.',
  'not_in' => 'Das ausgewählte :attribute ist nicht valide.',
  'numeric' => 'Das :attribute muss eine Zahl sein.',
  'present' => 'Das :attribute muss gegeben sein.',
  'regex' => 'Das :attribute Format ist invalide.',
  'required' => 'Das :attribute Feld ist erforderlich.',
  'required_if' => 'Das :attribute ist erforderlich, wenn :other ist :value.',
  'required_unless' => 'Das :attribute ist erforderlich, außer :other ist :values.',
  'required_with' => 'Das :attribute Feld ist erforderlich, wenn :values gegeben sind.',
  'required_with_all' => 'Das :attribute Feld ist erforderlich, wenn :values gegeben sind.',
  'required_without' => 'Das :attribute Feld ist erforderlich, wenn :values nicht gegeben sind.',
  'required_without_all' => 'Das :attribute Feld ist erforderlich, wenn keine der :values gegeben sind.',
  'same' => 'Das :attribute und :other müssen passen.',
  'size' => 
  array (
    'array' => 'Das :attribute muss Elemente der Größe :size beinhalten.',
    'file' => 'Das :attribute muss :size Kilobytes groß sein.',
    'numeric' => 'Das :attribute muss :size groß sein.',
    'string' => 'Das :attribute muss :size Zeichen groß sein.',
  ),
  'string' => 'Das :attribute muss ein String sein.',
  'timezone' => 'Das :attribute muss eine valide Zone sein.',
);
