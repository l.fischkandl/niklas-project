<?php

return array (
  'failed' => 'Diese Anmeldeinformationen stimmen nicht mit unseren Einträgen überein.',
  'throttle' => 'Zu viele Login Versuche. Bitte versuchen Sie es in :seconds Sekunden erneut.',
);
