<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-gears"></i>
                                    <span class="caption-subject bold uppercase"> <?php echo e(app('translator')->getFromJson('menu.generalSettings')); ?> </span>
                                </div>
                                <div class="actions">
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <?php echo Form::open(['url' => '', 'method' => 'POST', 'id' => 'add-edit-form', 'enctype' => 'multipart/form-data']); ?>

                                <input type="hidden" name="setting" value="general">
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input">
                                        <input name = "site_name" id="site_name" type="text" class="form-control"  value = "<?php echo e(isset($global->site_name) ? $global->site_name : ''); ?>"/>
                                        <label for="form_control_1"><?php echo e(app('translator')->getFromJson('menu.siteName')); ?></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input name = "name" id="name" type="text" class="form-control"  value = "<?php echo e(isset($global->name) ? $global->name : ''); ?>"/>
                                        <label for="form_control_1"><?php echo e(app('translator')->getFromJson('core.name')); ?></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input name = "email" id="email" type="email" class="form-control"  value = "<?php echo e(isset($global->email) ? $global->email : ''); ?>"/>
                                        <label for="form_control_1"><?php echo e(app('translator')->getFromJson('core.email')); ?></label>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        <select class="page-header-option form-control" name = "language" disabled>
					<option value="de" <?php if($global->locale == 'de'): ?> selected <?php endif; ?>>Deutsch</option>
                                        </select>
                                        <label for="form_control_1"><?php echo e(app('translator')->getFromJson('core.changeLanguage')); ?></label>
                                    </div>

                                    <div class="form-group ">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">

                                                <img src="<?php echo e($global->logo != null ? asset('/logo/'.$global->logo) : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA'); ?>" alt="" /> </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                                            <div>
                                                    <span class="btn btn-file">
                                                    <span class="fileinput-new"> <?php echo e(app('translator')->getFromJson('core.selectImage')); ?> </span>
                                                    <span class="fileinput-exists"> <?php echo e(app('translator')->getFromJson('core.change')); ?> </span>
                                                    <input type="file" name="image" id="image"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">  <?php echo e(app('translator')->getFromJson('core.remove')); ?>  </a>
                                            </div>
                                        </div>
                                        <div class="clearfix margin-top-10">
                                            <span class="label label-danger"><?php echo e(app('translator')->getFromJson('core.note')); ?>!</span><?php echo e(app('translator')->getFromJson('messages.imagePreviewNote')); ?></div>
                                    </div>

                                    <div class="form-actions noborder">
                                        <button type="button" class="btn  blue" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>

                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-footer'); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>