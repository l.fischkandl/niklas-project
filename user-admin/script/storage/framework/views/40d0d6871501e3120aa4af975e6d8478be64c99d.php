<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo app('translator')->getFromJson('menu.socialSettings'); ?></h3>
                            </div>
                            <?php echo Form::open(['url' => '', 'method' => 'post', 'id' => 'add-edit-form']); ?>

                            <input type="hidden" name="_method" value="PUT">
                            <div class="box-body">
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.facebookClientId'); ?></label>
                                    <input name = "facebook_client_id" type="text" class="form-control"  value="<?php echo e(isset($global->facebook_client_id) ? $global->facebook_client_id : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.facebookClientSecret'); ?></label>
                                    <input name = "facebook_client_secret" type="text" class="form-control"  value="<?php echo e(isset($global->facebook_client_secret) ? $global->facebook_client_secret : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.googleClientId'); ?></label>
                                    <input name = "google_client_id" type="text" class="form-control"  value="<?php echo e(isset($global->google_client_id) ? $global->google_client_id : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.googleClientSecret'); ?></label>
                                    <input name = "google_client_secret" type="text" class="form-control"  value="<?php echo e(isset($global->google_client_secret) ? $global->google_client_secret : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.twitterClientId'); ?></label>
                                    <input name = "twitter_client_id" type="text" class="form-control"  value="<?php echo e(isset($global->twitter_client_id) ? $global->twitter_client_id : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.twitterClientSecret'); ?></label>
                                    <input name = "twitter_client_secret" type="text" class="form-control"  value="<?php echo e(isset($global->twitter_client_secret) ? $global->twitter_client_secret : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.recaptchaPublicKey'); ?></label>
                                    <input name = "recaptcha_public_key" type="text" class="form-control"  value="<?php echo e(isset($global->recaptcha_public_key) ? $global->recaptcha_public_key : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.recaptchaPrivateKey'); ?></label>
                                    <input name = "recaptcha_private_key" type="text" class="form-control"  value="<?php echo e(isset($global->recaptcha_private_key) ? $global->recaptcha_private_key : ''); ?>">
                                </div>
                                <input type="hidden" name="setting" value="social">
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo app('translator')->getFromJson('core.submit'); ?></button>

                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts-footer'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>