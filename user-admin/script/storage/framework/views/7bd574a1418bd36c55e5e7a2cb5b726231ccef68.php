<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <i class="icon-<?php echo e(isset($iconEdit) ? $iconEdit : $icon); ?> font-red-sunglo"></i>
            <span class="caption-subject bold uppercase">
         <?php if(isset($permissions->id)): ?> <?php echo e(app('translator')->getFromJson('menu.editPermission')); ?><?php else: ?> <?php echo e(app('translator')->getFromJson('menu.addPermission')); ?> <?php endif; ?>
            </span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>

<div class="portlet-body form">
<?php echo Form::open(['url' => '' ,'method' => 'post', 'id' => 'add-edit-form','class'=>'form-horizontal']); ?>

    <?php if(isset($permissions->id)): ?> <input type="hidden" name="_method" value="PUT"> <?php endif; ?>
    <div class="box-body form">
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.name')); ?></label>
                <div class="col-sm-10">
                    <input type="text" <?php if(isset($permissions) && in_array($permissions->name,$permdata)): ?> readonly <?php endif; ?> name="name" id="name" class="form-control"  value="<?php echo e(isset($permissions->name) ? $permissions->name : ''); ?>" placeholder="<?php echo e(app('translator')->getFromJson('core.enterPermissionName')); ?>">

                    <div class="form-control-focus"> </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-sm-2 control-label" for="display_name"><?php echo e(app('translator')->getFromJson('core.displayName')); ?></label>
                <div class="col-sm-10">
                    <input type="text" id="display_name" class="form-control"  value="<?php echo e(isset($permissions->display_name) ? $permissions->display_name : old('display_name')); ?>" name="display_name" placeholder="<?php echo e(app('translator')->getFromJson('core.displayName')); ?>">
                    <div class="form-control-focus"> </div>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-sm-2 control-label" for="description"><?php echo e(app('translator')->getFromJson('core.description')); ?></label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="description" id="description"><?php echo e(isset($permissions->description) ? $permissions->description : old('description')); ?></textarea>
                    <div class="form-control-focus"> </div>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>

<div class="modal-footer">
    <button class="btn  dark " data-dismiss="modal" aria-hidden="true"><?php echo e(app('translator')->getFromJson('core.close')); ?></button>
    <button id="save" type="submit" class="btn  green" onclick="knap.addUpdate('permissions', '<?php echo e(isset($permissions->id) ? $permissions->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>
</div>
<?php echo e(Form::close()); ?>

</div>
</div>

