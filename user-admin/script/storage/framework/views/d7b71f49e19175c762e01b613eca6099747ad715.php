<?php $__env->startSection('content'); ?>

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <?php if($user->user_type == 'admin'): ?>
                <section class="content">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo e($activeUsers); ?>"><?php echo e($activeUsers); ?></span>
                                    </div>
                                    <div class="desc"><?php echo app('translator')->getFromJson('core.activeUsers'); ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo e($inActiveUsers); ?>"><?php echo e($inActiveUsers); ?></span></div>
                                    <div class="desc"><?php echo app('translator')->getFromJson('core.inActiveUsers'); ?></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo e($totalUsers); ?>"><?php echo e($totalUsers); ?></span>
                                    </div>
                                    <div class="desc"> <?php echo app('translator')->getFromJson('core.totalUsers'); ?> </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-key"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo e($rolesCount); ?>"><?php echo e($rolesCount); ?></span>
                                    </div>
                                    <div class="desc"> <?php echo app('translator')->getFromJson('core.role'); ?> </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-key"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo e($permissionCount); ?>"><?php echo e($permissionCount); ?></span></div>
                                    <div class="desc"> <?php echo app('translator')->getFromJson('core.permissions'); ?> </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>
            <?php elseif($user->can('view-users')): ?>
                <div class="row">
                    <div class="col-lg-6 col-xs-12 col-sm-12">
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-users font-green"></i>
                                    <span class="caption-subject font-green bold uppercase"><?php echo app('translator')->getFromJson('core.recentUsers'); ?></span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="mt-element-card mt-element-overlay">
                                    <div class="row">
                                        <?php $__empty_1 = true; $__currentLoopData = $recentUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recentUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                <div class="mt-card-item">
                                                    <div class="mt-card-avatar mt-overlay-1 mt-scroll-left">
                                                        <img src="<?php echo e($recentUser->getGravatarAttribute(250)); ?>" height="80px">
                                                    </div>
                                                    <div class="mt-card-content">
                                                        <h3 class="mt-card-name"><?php echo e($recentUser->trim_name); ?></h3>
                                                        <p class="mt-card-desc font-grey-mint">
                                                            <?php echo e($recentUser->created_at->diffForHumans()); ?>

                                                        </p>
                                                        <p class="mt-card-desc font-grey-mint">
                                                            <span id="status99" class="label <?php if($recentUser->gender == 'female' ): ?>bg-female  <?php else: ?> bg-blue <?php endif; ?> disabled " style="background: deeppink;">
                                                                <i class="<?php if($recentUser->gender == 'female' ): ?>fa fa-female <?php else: ?> fa fa-male <?php endif; ?>"></i> <?php if($recentUser->gender == 'female' ): ?><?php echo app('translator')->getFromJson('core.female'); ?> <?php else: ?> <?php echo app('translator')->getFromJson('core.male'); ?> <?php endif; ?>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <?php echo app('translator')->getFromJson('core.noUserFound'); ?>
                                        <?php endif; ?>

                                        <div class="col-lg-12 text-center">

                                            <a href="<?php echo e(route('users.index')); ?>" class="uppercase btn btn-default">View All Users</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <section class="content">
                    <div class="row">
                        <h4> <?php echo app('translator')->getFromJson('messages.welcomeToDashboard'); ?></h4>
                    </div>
                </section>

            <?php endif; ?>

        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>