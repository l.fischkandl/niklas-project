<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/icheck/skins/all.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <ol class="breadcrumb">
                <li><a href="#"><?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="panel panel-default">

        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="sessions">
                <thead>
                <tr>
                    <th width="20%"><?php echo app('translator')->getFromJson('core.user'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.ip_address'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.user_agent'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.last_activity'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.actions'); ?></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('modals'); ?>
    <?php echo $__env->make($global->theme_folder.'.include.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/icheck/icheck.min.js')); ?>"></script>
    <script type="text/javascript">
    
        var  table = $('#sessions').dataTable({
            "sScrollY": "100%",
            "scrollCollapse": true,
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            "ajax": "<?php echo e(route('get-sessions')); ?>",
            "columns": [
                {data: 'name', name: 'users.name'},
                {data: 'ip_address', name: 'ip_address'},
                {data: 'user_agent', name: 'user_agent'},
                {data: 'last_activity', name: 'last_activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "oLanguage": {
                "sProcessing": '<div class="overlay"><div class="cssload-speeding-wheel"></div></div>'
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>