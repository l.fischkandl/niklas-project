<?php $__env->startSection('content'); ?>

    <?php if($user->user_type == 'admin'): ?>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?php echo e($activeUsers); ?></h3>

                    <p><?php echo app('translator')->getFromJson('core.activeUsers'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo e($inActiveUsers); ?></h3>

                    <p><?php echo app('translator')->getFromJson('core.inActiveUsers'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo e($totalUsers); ?></h3>

                    <p><?php echo app('translator')->getFromJson('core.totalUsers'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?php echo e($rolesCount); ?></h3>

                    <p><?php echo app('translator')->getFromJson('core.role'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-key"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo e($permissionCount); ?></h3>

                    <p><?php echo app('translator')->getFromJson('core.permissions'); ?></p>
                </div>
                <div class="icon">
                    <i class="ion ion-key"></i>
                </div>
            </div>
        </div>

    </div>
    <?php elseif($user->can('view-users')): ?>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo app('translator')->getFromJson('core.recentUsers'); ?></h3>
                    <div class="box-tools pull-right">
                        <span class="label label-danger"><?php echo e(count($recentUsers)); ?> New Users</span>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        <!-- /.users-list -->
                        <?php $__empty_1 = true; $__currentLoopData = $recentUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recentUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <li>
                                <img src="<?php echo e($recentUser->getGravatarAttribute(250)); ?>" alt="User Image" height="80px">
                                <a class="users-list-name" href="#"><?php echo e($recentUser->name); ?></a>
                                <span class="users-list-date"><?php echo e($recentUser->created_at->diffForHumans()); ?></span>
                                <span class="label <?php if($recentUser->gender == 'female' ): ?>bg-red <?php else: ?> bg-blue <?php endif; ?>">
                                <i class="fa fa-<?php echo e($recentUser->gender); ?>"></i>
                                    <?php echo app('translator')->getFromJson('core.'.$recentUser->gender); ?>
                            </span>

                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                            <?php echo app('translator')->getFromJson('core.noUserFound'); ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="<?php echo e(route('users.index')); ?>" class="uppercase">View All Users</a>
                </div>
            </div>
            <!--/.box -->
        </div>
    </div>
    <?php else: ?>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
        <h4><?php echo app('translator')->getFromJson('messages.welcomeToDashboard'); ?></h4>
        </div>
    </div>
    <?php endif; ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>