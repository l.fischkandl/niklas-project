<?php $__env->startSection('style'); ?>
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-gears"></i>
                                    <span class="caption-subject bold uppercase"> <?php echo e(app('translator')->getFromJson('menu.mailSettings')); ?> </span>
                                </div>
                                <div class="actions">
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <?php echo Form::open(['url' => '', 'method' => 'post', 'id' => 'add-edit-form', 'class' => 'form-horizontal']); ?>

                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailDriver')); ?></label>
                                        <div class="col-md-9">
                                            <input name = "mailDriver" type="text" class="form-control"  value = "<?php echo e(isset($global->mail_driver) ? $global->mail_driver : ''); ?>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailHost')); ?></label>
                                        <div class="col-md-9">
                                            <input name = "mailHost" type="text" class="form-control"  value = "<?php echo e(isset($global->mail_host) ? $global->mail_host : ''); ?>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailPort')); ?></label>
                                        <div class="col-md-9">
                                            <input name = "mailPort" type="text" class="form-control"  value = "<?php echo e(isset($global->mail_port) ? $global->mail_port : ''); ?>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailUsername')); ?></label>
                                        <div class="col-md-9">
                                            <input name = "mailUsername" type="text" class="form-control"  value = "<?php echo e(isset($global->mail_username) ? $global->mail_username : ''); ?>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailPassword')); ?></label>
                                        <div class="col-md-9">
                                            <input name = "mailPassword" type="password" class="form-control"  value = "<?php echo e(isset($global->mail_password) ? $global->mail_password : ''); ?>">
                                            <div class="form-control-focus"> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.mailEncryption')); ?></label>
                                        <div class="col-md-9">
                                            <select class="page-header-option form-control" name = "mailEncryption">
                                                <option <?php if($global->mail_encryption == 'tls'): ?> selected <?php endif; ?> value="tls" selected="selected"><?php echo e(app('translator')->getFromJson('core.tls')); ?></option>
                                                <option <?php if($global->mail_encryption == 'ssl'): ?> selected <?php endif; ?>  value="ssl"><?php echo e(app('translator')->getFromJson('core.ssl')); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="setting" value="mail">
                                    <div class="form-actions noborder">
                                        <button type="button" class="btn  blue" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>

                                    </div>
                                </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <script src="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-footer'); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>