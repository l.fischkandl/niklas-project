<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo app('translator')->getFromJson('menu.generalSettings'); ?></h3>
                            </div>
                            <?php echo Form::open(['url' => '', 'method' => 'POST', 'id' => 'add-edit-form']); ?>

                            <input type="hidden" name="_method" value="PUT">
                            <div class="box-body">
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('menu.siteName'); ?></label>
                                    <input name = "site_name" type="text" class="form-control"  value="<?php echo e(isset($global->site_name) ? $global->site_name : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.name'); ?></label>
                                    <input name = "name" type="text" class="form-control"  value="<?php echo e(isset($global->name) ? $global->name : ''); ?>">
                                </div>
                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.email'); ?></label>
                                    <input name = "email" type="email" class="form-control"  value="<?php echo e(isset($global->email) ? $global->email : ''); ?>">
                                </div>

                                <div class="form-group">
                                    <label><?php echo app('translator')->getFromJson('core.changeLanguage'); ?></label>
                                    <select class="page-header-option form-control" name = "language">
                                        <option value="en" <?php if($global->locale == 'en'): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('core.english'); ?></option>
                                        <option value="es" <?php if($global->locale == 'es'): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('core.spanish'); ?></option>
                                        <option value="fr" <?php if($global->locale == 'fr'): ?> selected <?php endif; ?>><?php echo app('translator')->getFromJson('core.french'); ?></option>
                                        <option value="ar" <?php if($global->locale == 'ar'): ?> selected <?php endif; ?>>Arabic</option>
                                    </select>
                                </div>

                                <div class="form-group ">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">

                                            <img src="<?php echo e($global->logo != null ? asset('/logo/'.$global->logo) : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA'); ?>" alt="" /> </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px;"> </div>
                                        <div>
                                                    <span class="btn btn-file btn-default">
                                                    <span class="fileinput-new"> <?php echo app('translator')->getFromJson('core.selectImage'); ?> </span>
                                                    <span class="fileinput-exists"> <?php echo app('translator')->getFromJson('core.change'); ?> </span>
                                                    <input type="file" name="image" id="image"> </span>
                                            <a href="javascript:;" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">  <?php echo app('translator')->getFromJson('core.remove'); ?>  </a>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger"><?php echo app('translator')->getFromJson('core.note'); ?>!</span> <?php echo app('translator')->getFromJson('messages.imagePreviewNote'); ?> </div>
                                </div>

                                <input type="hidden" name="setting" value="general">
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo app('translator')->getFromJson('core.submit'); ?></button>

                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-footer'); ?>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-fileinput/bootstrap-fileinput.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>