<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datepicker/datepicker3.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
            <li class="active"><?php echo app('translator')->getFromJson('menu.session'); ?></li>
        </ol>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body ">
                    <table id="sessions" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr>
                        <tr>
                            <th width="20%"><?php echo app('translator')->getFromJson('core.user'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.ip_address'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.user_agent'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.last_activity'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.actions'); ?></th>
                        </tr>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('modals'); ?>
    <?php echo $__env->make($global->theme_folder.'.include.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
    <script type="text/javascript">

        
        var  table = $('#sessions').dataTable({
            "sScrollY": "100%",
            "scrollCollapse": true,
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, "desc"]
            ],
            "lengthMenu": [
                [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
            ],
            "ajax": "<?php echo e(route('get-sessions')); ?>",
            "columns": [
                {data: 'name', name: 'users.name'},
                {data: 'ip_address', name: 'ip_address'},
                {data: 'user_agent', name: 'user_agent'},
                {data: 'last_activity', name: 'last_activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "oLanguage": {
                "sProcessing": '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>