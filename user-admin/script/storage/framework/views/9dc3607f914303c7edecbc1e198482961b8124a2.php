<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="fa fa-gears"></i>
                                    <span class="caption-subject bold uppercase"><?php echo e(app('translator')->getFromJson('menu.socialSettings')); ?></span>
                                </div>
                                <div class="actions">
                                </div>
                            </div>
                            <div class="portlet-body box-body form">
                                <?php echo Form::open(['url' => '', 'method' => 'post', 'id' => 'add-edit-form','class'=>'form-hrizontal']); ?>

                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-body">
                                        <h4><strong><?php echo e(app('translator')->getFromJson('core.facebook')); ?></strong></h4>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.facebookClientId')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="facebook_client_id" id="facebook_client_id" class="form-control"   value = "<?php echo e(isset($global->facebook_client_id) ? $global->facebook_client_id : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.facebookClientSecret')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="facebook_client_secret" id="facebook_client_secret" class="form-control"   value = "<?php echo e(isset($global->facebook_client_secret) ? $global->facebook_client_secret : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <br>
                                        <h4><strong><?php echo e(app('translator')->getFromJson('core.google')); ?></strong></h4>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.googleClientId')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="google_client_id" id="google_client_id" class="form-control"   value = "<?php echo e(isset($global->google_client_id) ? $global->google_client_id : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.googleClientSecret')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="google_client_secret" id="google_client_secret" class="form-control"   value = "<?php echo e(isset($global->google_client_secret) ? $global->google_client_secret : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <br>
                                        <h4><strong><?php echo e(app('translator')->getFromJson('core.twitter')); ?></strong></h4>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.twitterClientId')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="twitter_client_id" id="twitter_client_id" class="form-control"   value = "<?php echo e(isset($global->twitter_client_id) ? $global->twitter_client_id : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.twitterClientSecret')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="twitter_client_secret" id="twitter_client_secret" class="form-control"   value = "<?php echo e(isset($global->twitter_client_secret) ? $global->twitter_client_secret : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <br>
                                        <h4><strong>Recaptcha</strong></h4>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.recaptchaPublicKey')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="recaptcha_public_key" id="recaptch_public_key" class="form-control"   value = "<?php echo e(isset($global->recaptcha_public_key) ? $global->recaptcha_public_key : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.recaptchaPrivateKey')); ?></label>
                                            <div class="col-md-6">
                                                <input type="text" name="recaptcha_private_key" id="recaptcha_private_key" class="form-control"   value = "<?php echo e(isset($global->recaptcha_private_key) ? $global->recaptcha_private_key : ''); ?>">
                                                <div class="form-control-focus"> </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" name="setting" value="social">
                                        <div class="form-actions noborder">
                                            <button type="button" class="btn  blue" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>

                                        </div>
                                    </div>
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-footer'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>