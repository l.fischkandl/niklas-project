<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <ul class="sidebar-menu">
            <li class="header"><?php echo e(app('translator')->getFromJson('menu.main_navigation')); ?></li>
            <li <?php if(strpos(\Request::route()->getName(),'dashboard') !== false): ?> class="active" <?php endif; ?>>
                <a href="<?php echo e(route('dashboard.index')); ?>">
                    <i class="fa fa-dashboard"></i> <span><?php echo e(app('translator')->getFromJson('menu.dashboard')); ?></span>
                </a>
            </li>
            <?php if (\Entrust::can('view-users')) : ?>
                <li <?php if(strpos(\Request::route()->getName(),'users') !== false ): ?> class="active" <?php endif; ?>>
                    <a href="<?php echo e(route('users.index')); ?>">
                        <i class="fa fa-users"></i> <span><?php echo e(app('translator')->getFromJson('menu.users')); ?></span>
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can(['view-role', 'view-permission'])) : ?>
                <li class="treeview <?php if(preg_match('/roles/', \Request::route()->getName()) or preg_match('/permissions/', \Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="#">
                        <i class="fa fa-key"></i> <span><?php echo e(app('translator')->getFromJson('menu.rolesPermissions')); ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Entrust::can('view-role')) : ?>
                            <li class="<?php if(preg_match('/roles/', \Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('roles.index')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.roles')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('view-permission')) : ?>
                            <li class="<?php if(preg_match('/permissions/', \Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('permissions.index')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.permissions')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                    </ul>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can('view-activity-log')) : ?>
                <li class="<?php if(preg_match('/activity/',\Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('activity')); ?>">
                        <i class="fa fa-clock-o"></i> <span><?php echo e(app('translator')->getFromJson('menu.activityLog')); ?></span>
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can('view-email-template')) : ?>
                <li <?php if(preg_match('/email-templates/',\Request::route()->getName())): ?> class="active" <?php endif; ?>>
                    <a href="<?php echo e(route('email-templates.index')); ?>">
                        <i class="fa fa-envelope"></i> <span><?php echo e(app('translator')->getFromJson('menu.emailTemplates')); ?></span>
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <li <?php if(strpos(\Request::route()->getName(),'chat')): ?> class="active" <?php endif; ?>>
                <a href="<?php echo e(route('chat')); ?>">
                    <i class="fa fa-comments-o"></i> <span><?php echo e(app('translator')->getFromJson('menu.userChat')); ?></span>
                </a>
            </li>
            <?php if($user->user_type=='admin'): ?>
                <li class="<?php if(preg_match('/session/',\Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('sessions.index')); ?>" class="nav-link ">
                        <i class="fa fa-sign-out"></i>
                        <span><?php echo e(app('translator')->getFromJson('menu.session')); ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (\Entrust::can(['update-social-settings', 'update-general-settings', 'update-theme-settings', 'update-mail-settings', 'update-common-settings', 'manage-custom-fields' ])) : ?>
                <li class=" <?php if(strpos(\Request::route()->getName(),'setting') or preg_match('/custom-fields/', \Request::route()->getName())): ?> active <?php endif; ?> treeview">
                    <a href="#">
                        <i class="fa fa-cog"></i> <span><?php echo e(app('translator')->getFromJson('menu.settings')); ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Entrust::can('update-social-settings')) : ?>
                            <li class="<?php if(preg_match('/social/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('social-settings')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.social')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-general-settings')) : ?>
                            <li class="<?php if(preg_match('/general/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('general-settings')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.general')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-theme-settings')) : ?>
                            <li class="<?php if(preg_match('/theme/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('theme-settings')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.theme')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('manage-custom-fields')) : ?>
                            <li class="<?php if(preg_match('/custom-fields/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('custom-fields.index')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.custom_fields')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-common-settings')) : ?>
                            <li class="<?php if(preg_match('/common-settings/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('common-settings')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.settings')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-mail-settings')) : ?>
                            <li class="<?php if(preg_match('/mail-settings/',\Request::route()->getName())): ?> active <?php endif; ?>"><a href="<?php echo e(route('mail-settings')); ?>"><i class="fa fa-circle-o"></i> <?php echo e(app('translator')->getFromJson('menu.mailSettings')); ?></a></li>
                        <?php endif; // Entrust::can ?>
                    </ul>
                </li>
            <?php endif; // Entrust::can ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>