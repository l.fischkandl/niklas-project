<?php $__env->startSection('style'); ?>
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="fa fa-gears"></i>
                                <span class="caption-subject bold uppercase"> <?php echo e(app('translator')->getFromJson('menu.settings')); ?> </span>
                            </div>
                            <div class="actions">
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <?php echo Form::open(['url' => '', 'method' => 'post', 'id' => 'add-edit-form', 'class' => 'form-horizontal']); ?>

                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.emailNotification')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox"  <?php if($global->email_notification == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal" name="emailNotification" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.recaptcha')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->recaptcha == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal" name="recaptcha" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.rememberMe')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->remember_me == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal" name="rememberMe" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.forgetPassword')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->forget_password == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal"  name="forgetPassword" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.allowRegister')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->allow_register == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal"  name="allowRegister" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.emailConfirmation')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->email_confirmation == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal"  name="emailConfirmation" value="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo e(app('translator')->getFromJson('core.customFieldOnRegister')); ?></label>
                                    <div class="col-md-9">
                                        <input type="checkbox" <?php if($global->custom_field_on_register == 1): ?> checked <?php endif; ?> class="make-switch" data-size="normal"  name="customOnRegister" value="1">
                                    </div>
                                </div>

                                <input type="hidden" name="setting" value="settings">
                                <div class="form-actions noborder">
                                    <button type="button" class="btn  blue" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>

                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </section>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <script src="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts-footer'); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>