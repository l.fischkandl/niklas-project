<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <i class="icon-<?php echo e($iconEdit); ?> font-red-sunglo"></i>
            <span class="caption-subject bold uppercase">
         <?php echo e(app('translator')->getFromJson('menu.editEmailTemplate')); ?>
            </span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="portlet-body form">
        <?php echo Form::open(['url' => '','class'=>'form-horizontal' ,'autocomplete'=>'off','id'=>'add-edit-form']); ?>

        <?php if(isset($editTemplate->id)): ?> <input type="hidden" name="_method" value="PUT"> <?php endif; ?>
        <div class="box-body form">
            <div class="form-body">
                <div class="form-group form-md-line-input">
                    <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.id')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="id" id="id" class="form-control"  placeholder="<?php echo e(app('translator')->getFromJson('core.id')); ?>" value="<?php echo e(isset($editTemplate->email_id) ? $editTemplate->email_id : ''); ?>" disabled>
                        <span class="form-control-focus"> </span>
                        <span class=" help-block help-block-error" style="color: #e73d4a;"><?php echo e(app('translator')->getFromJson('messages.youCantChangeId')); ?></span>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-sm-2 control-label" for="email"><?php echo e(app('translator')->getFromJson('core.subject')); ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="<?php echo e(app('translator')->getFromJson('core.subject')); ?>" value="<?php echo e(isset($editTemplate->subject) ? $editTemplate->subject : ''); ?>">
                        <div class="form-control-focus"> </div>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo e(app('translator')->getFromJson('core.body')); ?></label>
                    <div class="col-md-10">
                        <textarea name="body" class="form-control" rows="10" id="myEditor" placeholder="<?php echo e(app('translator')->getFromJson('core.enterText')); ?>"><?php echo isset($editTemplate->body) ? $editTemplate->body : ''; ?> </textarea>
                    </div>
                </div>

                <div class="form-group form-md-line-input">
                    <label class="col-sm-2 control-label"><?php echo e(app('translator')->getFromJson('core.variablesUsed')); ?></label>
                    <div class="col-sm-10">
                        <?php echo e(isset($emailVariables) ? $emailVariables : ''); ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn dark " data-dismiss="modal" aria-hidden="true"><?php echo e(app('translator')->getFromJson('core.close')); ?></button>
            <button id="save" type="submit" class="btn green" onclick="knap.addUpdate('email-templates', '<?php echo e(isset($editTemplate->id) ? $editTemplate->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>
        </div>
        <?php echo e(Form::close()); ?>

    </div>
</div>
