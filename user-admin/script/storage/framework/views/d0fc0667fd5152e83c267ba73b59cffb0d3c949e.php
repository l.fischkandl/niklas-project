<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/iCheck/minimal/_all.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <ol class="breadcrumb">
                <li><a href="#"><?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button id="sample_editable_1_new" class="btn btn-custom" onclick="knap.addModal('permissions.create');return false;">
                            <?php echo app('translator')->getFromJson('core.btnAddPermission'); ?> <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="permissions">
                <thead>
                <tr>
                    <th class="hidden"><?php echo app('translator')->getFromJson('core.id'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.name'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.displayName'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.description'); ?></th>
                    <th><?php echo app('translator')->getFromJson('core.actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('modals'); ?>
    <?php echo $__env->make($global->theme_folder.'.include.add-edit-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make($global->theme_folder.'.include.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script type="text/javascript">

var  table = $('#permissions').dataTable({
        "sScrollY": "100%",
    "scrollCollapse": true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo e(route('user.get-permissions')); ?>",
        "columns": [
            {data: 'id', name: 'id', orderable: false, searchable: false, visible:false},
            {data: 'name', name: 'name', orderable: true, searchable: true},
            {data: 'display_name', name: 'display_name', orderable: true, searchable: true},
            {data: 'description', name: 'description', orderable: true, searchable: true},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "oLanguage": {
            "sProcessing": '<div class="overlay"><div class="cssload-speeding-wheel"></div></div>'
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>