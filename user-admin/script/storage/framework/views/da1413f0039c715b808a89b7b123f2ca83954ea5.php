<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <i class="icon-<?php echo e($iconEdit); ?> font-red-sunglo"></i>
            <span class="caption-subject bold uppercase">
            <?php echo e(app('translator')->getFromJson('core.assignRole')); ?>
            </span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="portlet-body form">
        <?php echo Form::open(['url' => '','class'=>'form-horizontal' ,'autocomplete'=>'off','id'=>'assign-role']); ?>

        <div class="box-body form">
            <div class="form-body">
                <div class="form-group form-md-checkboxes">
                    <label class="col-md-2 control-label"><?php echo e(app('translator')->getFromJson('core.selectRoles')); ?></label>
                    <div class="col-md-10">
                        <div class="md-checkbox-inline">
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="md-checkbox">
                                    <input type="checkbox" id="id<?php echo e($role->id); ?>" value="<?php echo e($role->id); ?>" name="role[<?php echo e($role->id); ?>]" class="md-check" <?php if(isset($assignedRoles) && in_array($role->id, $assignedRoles)): ?> checked <?php endif; ?> >
                                    <label for="id<?php echo e($role->id); ?>">
                                        <span class="inc"></span>
                                        <span class="check"></span>
                                        <span class="box"></span> <?php echo e($role->display_name); ?> </label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo e($user->id); ?>">
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn  dark" data-dismiss="modal" aria-hidden="true"><?php echo e(app('translator')->getFromJson('core.close')); ?></button>
            <button id="save" type="submit" class="btn  green" onclick="knap.assignRole(<?php echo e(isset($user->id) ? $user->id : ''); ?>);return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>
        </div>
        <?php echo e(Form::close()); ?>

    </div>
</div>
