<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo e($global->site_name); ?> | <?php echo e($pageTitle); ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset($global->theme_folder.'/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo e(asset($global->theme_folder.'/css/animate.css')); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo e(asset($global->theme_folder.'/css/style.css')); ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo e(asset($global->theme_folder.'/css/colors/'.$global->theme_color.'.css')); ?>" id="theme"  rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/froiden-helper/helper.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')); ?>">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register register-form">
    <div class="login-box">
        <div class="white-box">
            <h3 class="box-title m-b-20"><?php echo app('translator')->getFromJson('core.signUp'); ?></h3>
            <!-- BEGIN REGISTRATION FORM -->
            <?php echo Form::open(['url' => '', 'method' => 'POST','class'=>'form-horizontal form-material', 'id' => 'register-form']); ?>

            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="text" placeholder="<?php echo app('translator')->getFromJson('core.name'); ?>" name="name" /> </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <input class="form-control placeholder-no-fix" type="text" placeholder="<?php echo app('translator')->getFromJson('core.email'); ?>" name="email" /> </div>
            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="<?php echo app('translator')->getFromJson('core.password'); ?>" name="password" /> </div>

            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo app('translator')->getFromJson('core.rePassword'); ?>" name="password_confirmation" />
            </div>

            <div class="form-group">
                <input type="date" class="form-control form-control-inline date-picker" size="16" name="dob"
                       id="datepicker" placeholder="<?php echo app('translator')->getFromJson('dob'); ?>" value="">
            </div>
            <div class="form-group">

                    <div class="radio radio-success">
                        <input type="radio" name="gender" id="male" class="md-radio" value="male" checked>
                        <label for="male">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span> <?php echo app('translator')->getFromJson('core.male'); ?>
                        </label>
                    </div>
                    <div class="radio radio-success">
                        <input type="radio" name="gender" id="female" class="md-radio" value="female">
                        <label for="female">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span> <?php echo app('translator')->getFromJson('core.female'); ?>
                        </label>
                    </div>

            </div>

            
            <?php if($global->custom_field_on_register == 1): ?>
                <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="form-group ">
                        <?php if( $field->type == 'text'): ?>
                            <input type="text" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">
                        <?php elseif($field->type == 'password'): ?>
                            <input type="password" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">
                        <?php elseif($field->type == 'number'): ?>
                            <input type="number" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">

                        <?php elseif($field->type == 'textarea'): ?>
                            <textarea name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" id="<?php echo e($field->name); ?>" cols="3" placeholder="<?php echo e(ucwords($field->name)); ?>"><?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?></textarea>

                        <?php elseif($field->type == 'radio'): ?>
                        <label  class="control-label"><?php echo e($field->label); ?></label>
                            <div class="md-radio-list">

                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="radio radio-success">
                                        <input type="radio" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" id="optionsRadios<?php echo e($key.$field->id); ?>"
                                               <?php if($key==0): ?> checked <?php endif; ?>     class="md-radio" value="<?php echo e($value); ?>">
                                        <label for="optionsRadios<?php echo e($key.$field->id); ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <?php echo e($value); ?>

                                        </label>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php elseif($field->type == 'select'): ?>
                        <label  class="control-label"><?php echo e($field->label); ?></label>
                            <?php echo Form::select($field->name,
                                    $field->values,
                                     isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender']); ?>


                        <?php elseif($field->type == 'checkbox'): ?>
                        <label  class="control-label"><?php echo e($field->label); ?></label>
                            <div class="mt-checkbox-inline">
                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>][]" type="checkbox" value="<?php echo e($key); ?>"> <?php echo e($value); ?>

                                        <span></span>
                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php elseif($field->type == 'date'): ?>
                            <input type="text" class="form-control form-control-inline date-picker" size="16" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                   id="datepicker" value="<?php echo e(isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format('Y-m-d')); ?>">
                        <?php endif; ?>

                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>

            <?php if($global->recaptcha == 1): ?>
                <div class="form-group">
                    <label class="control-label"><?php echo app('translator')->getFromJson('core.captcha'); ?></label>
                    <?php echo \Greggilbert\Recaptcha\Facades\Recaptcha::render(); ?>

                </div>
            <?php endif; ?>

            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" onclick="knap.register('login-register');return false"><?php echo app('translator')->getFromJson('core.signUp'); ?></button>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p><?php echo app('translator')->getFromJson('messages.alreadyHaveAccount'); ?>? <a href="<?php echo e(route('user.login')); ?>" class="text-primary m-l-5"><b><?php echo app('translator')->getFromJson('core.signIn'); ?></b></a></p>
                </div>
            </div>

        <?php echo Form::close(); ?>

        <!-- END LOGIN FORM -->
        </div>
    </div>
</section>
<!-- jQuery -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/sidebar-nav/dist/sidebar-nav.min.js')); ?>"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/js/jquery.slimscroll.js')); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo e(asset($global->theme_folder.'/js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/js/custom.min.js')); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/laroute.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/knap.js')); ?>"></script>
<script>

    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });

</script>
</body>

</html>
