<footer class="footer text-center">
    <strong>Copyright &copy; <?php echo e(Carbon\Carbon::now()->format('Y')); ?> <?php echo e($global->site_name); ?></strong> <?php echo e(app('translator')->getFromJson('messages.allRightsReserved')); ?>
</footer>

<!-- Add FORM -->
<div id="AdminEditModal" class="modal fade" tabindex="-1"  data-backdrop="static" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel panel-inverse">
            
        </div>
    </div>
</div>