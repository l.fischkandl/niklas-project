<?php $__env->startSection('style'); ?>

    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/global/css/components-md.min.css')); ?>">
<?php $__env->stopSection(); ?>
<style>
    .contacts-list li.active{
       background: #000 !important;
   }
   .direct-chat-contacts {
       height:auto!important;
   }
    .slimScrollDiv{
        height: 293px !important;
    }

</style>
<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
            <li class="active"><?php echo app('translator')->getFromJson('menu.userChat'); ?></li>
        </ol>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box padding2em">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="box userListBox">
                            <div class="box-header">
                                <div class="caption">
                                    <i class="icon-bubbles font-dark"></i>
                                    <span class="caption-subject font-dark bold uppercase"><?php echo app('translator')->getFromJson('menu.users'); ?></span>
                                </div>

                            </div>
                            <div class="box-body main-header" id="userList">
                                <!-- Contacts are loaded here -->
                                <div class="direct-chat-contacts navbar" >
                                    <ul class="contacts-list userList" id="user_list" >
                                        <?php $__empty_1 = true; $__currentLoopData = $userList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <li id="dp_<?php echo e($users->id); ?>">
                                                <a href="javascript:;" data-toggle="tooltip" title="" onclick="knap.getChatData(<?php echo e($users->id); ?>, '<?php echo e(addslashes($users->name)); ?>');return false;" >
                                                    <img class="contacts-list-img" src="<?php echo e($users->getGravatarAttribute(250)); ?>" alt="User Image" style="height: 35px; width: 35px;">

                                                    <div class="contacts-list-info">
                                                    <span class="contacts-list-name">
                                                        <?php echo e($users->name); ?>

                                                    </span>
                                                    </div>
                                                    <!-- /.contacts-list-info -->
                                                </a>
                                            </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <li>
                                                <?php echo app('translator')->getFromJson('messages.noUserFound'); ?>
                                            </li>
                                            <?php endif; ?>
                                          <!-- End Contact Item -->
                                    </ul>
                                    <!-- /.contatcts-list -->
                                </div>
                                <!-- /.direct-chat-pane -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <!-- DIRECT CHAT DANGER -->
                        <div class="box direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title dpName" id="dpName"><?php echo e($dpName); ?></h3>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <!-- Conversations are loaded here -->
                                <div class="direct-chat-messages chats" style="padding: 1.5em !important;" id="chatsRecord">

                                </div>
                                <!--/.direct-chat-messages-->

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer" id="box-footer">
                               <?php echo Form::open(['url' => '' ,'method' => 'post',]); ?>

                                    <div class="input-group">
                                        <input type="text" name="message" id="submitTexts" placeholder="<?php echo app('translator')->getFromJson('core.typeYourMessage'); ?>" class="form-control">
                                        <input  id="dpID" value="<?php echo e($dpData); ?>"  type="hidden"  />
                                        <input  id="dpName" value="<?php echo e($dpName); ?>"  type="hidden"  />
                                              <span class="input-group-btn" >
                                                <button id="submitBtn" type="submit" onclick="knap.sendMessage('box-footer');return false;" class="btn btn-primary btn-flat"><?php echo app('translator')->getFromJson('core.send'); ?></button>
                                              </span>
                                    </div>
                                    </br>
                                <?php echo Form::close(); ?>

                                <div id="errorMessage"></div>
                            </div>
                            <!-- /.box-footer-->
                        </div>
                        <!--/.direct-chat -->
                    </div>
                </div>


                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>

    <!-- DataTables -->
    <script>

        $(function(){
            $('#userList').slimScroll({
                height: '250px'
            });
        });

        var dpButtonID = "";
        var dpName     = "";

        var dpClassID = '<?php echo e($dpData); ?>';

        if(dpClassID){ $('#dp_'+dpClassID).addClass('active');}
        //getting data
        knap.getChatData(dpButtonID , dpName);

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>