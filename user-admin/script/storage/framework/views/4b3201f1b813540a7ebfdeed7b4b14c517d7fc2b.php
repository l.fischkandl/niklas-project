<?php $__env->startSection('page-header'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <ol class="breadcrumb">
                <li><a href="#"><?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php if($user->user_type == 'admin'): ?>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title"><?php echo app('translator')->getFromJson('core.activeUsers'); ?></h3>
                    <ul class="list-inline two-part">
                        <li>
                            <i class="fa fa-users"></i>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success"><?php echo e($activeUsers); ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title"><?php echo app('translator')->getFromJson('core.inActiveUsers'); ?></h3>
                    <ul class="list-inline two-part">
                        <li>
                            <i class="fa fa-users"></i>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple"><?php echo e($inActiveUsers); ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title"><?php echo app('translator')->getFromJson('core.totalUsers'); ?></h3>
                    <ul class="list-inline two-part">
                        <li>
                            <i class="fa fa-users"></i>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-info"></i> <span class="counter text-info"><?php echo e($totalUsers); ?></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title"> <?php echo app('translator')->getFromJson('core.role'); ?> </h3>
                    <ul class="list-inline two-part">
                        <li>
                            <i class="fa fa-key"></i>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-success"></i> <span class="counter text-success"><?php echo e($rolesCount); ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box analytics-info">
                    <h3 class="box-title"><?php echo app('translator')->getFromJson('core.permissions'); ?></h3>
                    <ul class="list-inline two-part">
                        <li>
                            <i class="fa fa-key"></i>
                        </li>
                        <li class="text-right"><i class="ti-arrow-up text-purple"></i> <span class="counter text-purple"><?php echo e($permissionCount); ?></span></li>
                    </ul>
                </div>
            </div>
        </div>

    <?php elseif($user->can('view-users')): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-inverse block1">
                    <div class="panel-heading"><?php echo app('translator')->getFromJson('core.recentUsers'); ?></div>
                    <div class="panel-wrapper">
                        <div class="panel-body">
                            <div class="row el-element-overlay m-b-40">
                                <div class="col-md-12">


                                    <!-- /.usercard -->
                                    <?php $__empty_1 = true; $__currentLoopData = $recentUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recentUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                            <div class="white-box">
                                                <div class="el-card-item">
                                                    <div class="el-card-avatar el-overlay-1">
                                                        <img src="<?php echo e($recentUser->getGravatarAttribute(250)); ?>" height="80px"/>
                                                    </div>
                                                    <div class="el-card-content">
                                                        <h3 class="box-title"><?php echo e(explode(' ', $recentUser->name)[0]); ?></h3>
                                                        <small><?php echo e($recentUser->created_at->diffForHumans()); ?></small><br>
                                                        <small><span class="label <?php if($recentUser->gender == 'female' ): ?>bg-pink <?php else: ?> bg-info <?php endif; ?>">
                                <i class="fa fa-<?php echo e($recentUser->gender); ?>"></i>
                                                                <?php echo app('translator')->getFromJson('core.'.$recentUser->gender); ?>
                            </span></small><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                        <?php echo app('translator')->getFromJson('core.noUserFound'); ?>
                                    <?php endif; ?>

                                    <div class="col-lg-12 text-center">

                                        <a href="<?php echo e(route('users.index')); ?>" class="uppercase btn btn-default">View All Users</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php else: ?>
        <h4><?php echo app('translator')->getFromJson('messages.welcomeToDashboard'); ?></h4>
    <?php endif; ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>