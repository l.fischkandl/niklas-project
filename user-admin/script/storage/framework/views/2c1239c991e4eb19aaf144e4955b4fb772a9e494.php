<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/iCheck/minimal/_all.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo e(app('translator')->getFromJson('menu.themeSettings')); ?></h3>
                            </div>
                            <?php echo Form::open(['url' => '', 'method' => 'post', 'id' => 'add-edit-form', 'enctype' => 'multipart/form-data','class'=>'form-horizontal']); ?>

                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="setting" value="theme">
                            <div class="box-body">
                                <div class="form-group">
                                    <div class="col-sm-4 text-center">
                                        <?php echo e(app('translator')->getFromJson('core.metronic')); ?>
                                        <img id="metronic-img" src="<?php echo e(asset('theme-images/metronic'.(($global->theme_folder=='metronic')?'-'.$global->theme_color:'').'.jpg')); ?>" style="width: 85%;height: 100%;margin-top: 10px;">

                                    </div>
                                    <div class="col-sm-4  text-center">
                                        <ul class="list-unstyled clearfix">

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="metronic" value="metronic:default" <?php if($global->theme_folder=='metronic' && $global->theme_color=='default'): ?>) checked <?php endif; ?>>
                                                    <label for="metronic">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #4F5467;"></span>
                                                    <span style="display:block;background: #4F5467; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.darkHeader')); ?></p>

                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="metronic1" value="metronic:light" <?php if($global->theme_folder=='metronic' && $global->theme_color=='light'): ?>) checked <?php endif; ?>>
                                                    <label for="metronic1">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>

                                                </div>
                                                <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.lightHeader')); ?></p>


                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-2 text-center">
                                        RTL
                                        <select name="rtl" id="" class="form-control">
                                            <option value="0"><?php echo e(app('translator')->getFromJson('core.no')); ?></option>
                                            <option value="1" <?php if($global->rtl==1): ?> selected <?php endif; ?>><?php echo e(app('translator')->getFromJson('core.yes')); ?></option>

                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-4 text-center">
                                         <span>
                                            <span  style="margin-left: 5px;margin-right:10px;font-size: 13px;"><?php echo e(app('translator')->getFromJson('core.adminLte')); ?></span>
                                            <img id="admin-lte-img" src="<?php echo e(asset('theme-images/admin-lte'.(($global->theme_folder=='admin-lte')?'-'.$global->theme_color:'').'.jpg')); ?>" style="width: 80%;height: 100%;margin-top: 10px;">
                                         </span>
                                    </div>
                                    <div class="col-sm-6 text-center" id="admin-lte-group">
                                        <ul class="list-unstyled clearfix">

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte1" value="admin-lte:blue" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='blue'): ?> checked <?php endif; ?>>
                                                    <label for="lte1">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>

                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span>
                                                    <span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div><p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.blue')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte2" value="admin-lte:black" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='black'): ?> checked <?php endif; ?>>
                                                    <label for="lte2">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)"
                                                     class="clearfix">
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>
                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.white')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte3" value="admin-lte:purple" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='purple'): ?> checked <?php endif; ?>>
                                                    <label for="lte3">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span>
                                                    <span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.purple')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte4" value="admin-lte:green" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='green'): ?> checked <?php endif; ?>>
                                                    <label for="lte4">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span>
                                                    <span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>
                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.green')); ?></p>
                                            </li>


                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte5" value="admin-lte:red" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='red'): ?> checked <?php endif; ?>>
                                                    <label for="lte5">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span>
                                                    <span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>
                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.red')); ?></p>
                                            </li>


                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte6" value="admin-lte:yellow" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='yellow'): ?> checked <?php endif; ?>>
                                                    <label for="lte6">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span>
                                                    <span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>
                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.yellow')); ?></p>
                                            </li>


                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte7" value="admin-lte:blue-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='blue-light'): ?>checked <?php endif; ?>>
                                                    <label for="lte7">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span>
                                                    <span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin" style="font-size: 12px"><?php echo e(app('translator')->getFromJson('core.blueLight')); ?></p>
                                            </li>


                                            <li style="float:left; width: 33.33333%; padding: 5px;">

                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte8" value="admin-lte:black-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='black-light'): ?> checked <?php endif; ?>>
                                                    <label for="lte8">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>

                                                <div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix">
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin" style="font-size: 12px"><?php echo e(app('translator')->getFromJson('core.blackLight')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte9" value="admin-lte:purple-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='purple-light'): ?> checked <?php endif; ?>>
                                                    <label for="lte9">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span>
                                                    <span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>
                                                <p class="text-center no-margin" style="font-size: 12px"><?php echo e(app('translator')->getFromJson('core.purpleLight')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte10" value="admin-lte:green-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='green-light'): ?> checked <?php endif; ?>>
                                                    <label for="lte10">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span>
                                                    <span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin" style="font-size: 12px"><?php echo e(app('translator')->getFromJson('core.greenLight')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte11" value="admin-lte:red-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='red-light'): ?> checked <?php endif; ?>>
                                                    <label for="lte11">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span>
                                                    <span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin" style="font-size: 12px"><?php echo e(app('translator')->getFromJson('core.redLight')); ?></p>
                                            </li>

                                            <li style="float:left; width: 33.33333%; padding: 5px;">

                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="lte12" value="admin-lte:yellow-light" <?php if($global->theme_folder=='admin-lte' && $global->theme_color=='yellow-light'): ?> checked <?php endif; ?>>
                                                    <label for="lte12">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span>
                                                    <span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin" style="font-size: 12px;"><?php echo e(app('translator')->getFromJson('core.yellowLight')); ?></p></li>
                                        </ul>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-4 text-center">
                                         <span>
                                            <span  style="margin-left: 5px;margin-right:10px;font-size: 13px;"><?php echo e(app('translator')->getFromJson('core.eliteAdmin')); ?></span>
                                            <img id="elite-admin-img" src="<?php echo e(asset('theme-images/elite-admin'.(($global->theme_folder=='elite-admin')?'-'.$global->theme_color:'').'.jpg')); ?>" style="width: 80%;height: 100%;margin-top: 10px;">
                                         </span>
                                    </div>
                                    <div class="col-sm-6 text-center">
                                        <ul class="list-unstyled clearfix">

                                            
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite1" value="elite-admin:blue" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='blue'): ?> checked <?php endif; ?>>
                                                    <label for="elite1">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>

                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #3598dc;"></span>
                                                    <span class="bg-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_blue')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite2" value="elite-admin:default" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='default'): ?> checked <?php endif; ?>>
                                                    <label for="elite2">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #e7957d;"></span>
                                                    <span style="display:block;background: #e7957d; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_default')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite3" value="elite-admin:gray" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='gray'): ?> checked <?php endif; ?>>
                                                    <label for="elite3">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #a0aec4;"></span>
                                                    <span style="display:block;background: #a0aec4; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_gray')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite4" value="elite-admin:green" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='green'): ?> checked <?php endif; ?>>
                                                    <label for="elite4">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #00c292;"></span>
                                                    <span style="display:block;background: #00c292; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_green')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite5" value="elite-admin:megna" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='megna'): ?> checked <?php endif; ?>>
                                                    <label for="elite5">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #01c0c8;"></span>
                                                    <span style="display:block;background: #01c0c8; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_megna')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elite6" value="elite-admin:purple" <?php if($global->theme_folder=='elite-admin' && $global->theme_color=='purple'): ?> checked <?php endif; ?>>
                                                    <label for="elite6">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #ab8ce4;"></span>
                                                    <span style="display:block;background: #ab8ce4; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: white;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.light_purple')); ?></p>
                                            </li>

                                            
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark1" value="elite-admin:blue-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark1">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>

                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #3598dc;"></span>
                                                    <span class="bg-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_blue')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark2" value="elite-admin:default-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark2">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #e7957d;"></span>
                                                    <span style="display:block;background: #e7957d; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_default')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark3" value="elite-admin:gray-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark3">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #a0aec4;"></span>
                                                    <span style="display:block;background: #a0aec4; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_gray')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark4" value="elite-admin:green-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark4">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #00c292;"></span>
                                                    <span style="display:block;background: #00c292; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_green')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark5" value="elite-admin:megna-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark5">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #01c0c8;"></span>
                                                    <span style="display:block;background: #01c0c8; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_megna')); ?></p>
                                            </li>
                                            <li style="float:left; width: 33.33333%; padding: 5px;">
                                                <div class="md-radio">
                                                    <input type="radio" name="theme" class="md-radio" id="elitedark6" value="elite-admin:purple-dark" <?php if($global->theme_folder=='elite-admin'): ?> checked <?php endif; ?>>
                                                    <label for="elitedark6">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span>
                                                    </label>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 7px; background: #ab8ce4;"></span>
                                                    <span style="display:block;background: #ab8ce4; width: 80%; float: left; height: 7px;"></span>
                                                </div>
                                                <div>
                                                    <span style="display:block; width: 20%; float: left; height: 20px; background: #4f5467;"></span>
                                                    <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                                                </div>

                                                <p class="text-center no-margin"><?php echo e(app('translator')->getFromJson('core.dark_purple')); ?></p>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="box-footer text-center" style="clear: both;">
                                    <button type="button" class="btn btn-primary" onclick="knap.addUpdate('settings', '<?php echo e(isset($global->id) ? $global->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>
                                </div>
                            </div>
                            <?php echo Form::close(); ?>

                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </section>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts-footer'); ?>
    <script>

        // iCheck for checkbox and radio inputs
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });

        $('input').on('ifChecked', function(event){
            var path = "<?php echo e(asset('theme-images/')); ?>";
            var str = $(this).attr('value');
            var split = str.split(':');
            var img = split[1];
            $('#'+split[0]+'-img').attr("src", path+"/"+split[0]+"-"+img+".jpg");
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>