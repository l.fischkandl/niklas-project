<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datepicker/datepicker3.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/cropper/dist/cropper.min.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
            <li class="active"><?php echo app('translator')->getFromJson('menu.users'); ?></li>
        </ol>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a href="javascript: ;" onclick="knap.addModal('users.create');return false;" class="btn btn-success">
                        <span class="hidden-xs"> <?php echo app('translator')->getFromJson('core.btnAddUser'); ?> </span><i class="fa fa-plus"></i>
                    </a>

                    <a href="<?php echo e(route('user.export-users')); ?>" class="btn btn-success pull-right">
                        <span class="hidden-xs"><?php echo app('translator')->getFromJson('core.exportCsv'); ?></span> <i class="fa fa-file-excel-o"></i>
                    </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body ">
                    <table id="users" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th><?php echo app('translator')->getFromJson('core.id'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.avatar'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.name'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.email'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.gender'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.role'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.status'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.actions'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('modals'); ?>
    <?php echo $__env->make($global->theme_folder.'.include.add-edit-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make($global->theme_folder.'.include.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make($global->theme_folder.'.include.image-crop-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/cropper/dist/cropper.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/cropper/js/main.js')); ?>"></script>
    <script type="text/javascript">
                
        var  table = $('#users').dataTable({
                    "sScrollY": "100%",
                    "scrollCollapse": true,
                    "responsive": true,
                    "processing": true,
                    "serverSide": true,
                    "order": [
                        [0, "desc"]
                    ],
                    "lengthMenu": [
                        [10, 15, 20, -1],
                        [10, 15, 20, "All"] // change per page values here
                    ],
                    "ajax": "<?php echo e(route('user.get-users')); ?>",
                    "columns": [
                        {data: 'id', name: 'id'},
                        {data: 'avatar', name: 'avatar'},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'gender', name: 'gender'},
                        {data: 'roles', name: 'roles',searchable: false, orderable: false },
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    "oLanguage": {
                        "sProcessing": '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
                    }
                });


    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>