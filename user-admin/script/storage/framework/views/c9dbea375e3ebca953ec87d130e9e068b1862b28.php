<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <div class="row bg-title">
        <!-- .page title -->
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo e($pageTitle); ?></h4>
        </div>
        <!-- /.page title -->
        <!-- .breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            <ol class="breadcrumb">
                <li><a href="#"><?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
                <li class="active"><?php echo e($pageTitle); ?></li>
            </ol>
        </div>
        <!-- /.breadcrumb -->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <div class="panel panel-default">

        <div class="panel-body">
            <!-- .chat-row -->
            <div class="chat-main-box">
                <!-- .chat-left-panel -->
                <div class="chat-left-aside">
                    <div class="open-panel"><i class="ti-angle-right"></i></div>
                    <div class="chat-left-inner" >
                        <div class="form-material p-20">
                           </div>
                        <ul class="chatonline style-none" id="user_list">
                            <?php $__empty_1 = true; $__currentLoopData = $userList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <li  onclick="knap.getChatData(<?php echo e($users->id); ?>, '<?php echo e(addslashes($users->name)); ?>');return false;">
                                <a class="mt-comment" href="javascript:void(0)" id="dp_<?php echo e($users->id); ?>">
                                    <img src="<?php echo e($users->getGravatarAttribute(250)); ?>" alt="user-img" class="img-circle" style="height: 35px; width: 35px;"> <span><?php echo e($users->name); ?></span>
                                </a>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                <li>
                                    <?php echo app('translator')->getFromJson('messages.noUserFound'); ?>
                                </li>
                            <?php endif; ?>
                            <li class="p-20"></li>
                        </ul>
                    </div>
                </div>
                <!-- .chat-left-panel -->
                <!-- .chat-right-panel -->
                <div class="chat-right-aside">
                    <div class="chat-main-header">
                        <div class="p-20 b-b">
                            <h3 class="box-title dpName"  id="dpName"><?php echo e($dpName); ?></h3> </div>
                    </div>
                    <div class="chat-box">
                        <ul class="chat-list slimscroll p-t-30 chats" id="chatsRecord">

                        </ul>
                        <div class="row send-chat-box">
                            <?php echo Form::open(['url' => '' ,'method' => 'post',]); ?>

                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="submitTexts" name="message" placeholder="<?php echo app('translator')->getFromJson('core.typeYourMessage'); ?>">
                                    <input  id="dpID" value="<?php echo e($dpData); ?>"  type="hidden"  />
                                    <input  id="dpName" value="<?php echo e($dpName); ?>"  type="hidden"  />
                                    <div class="custom-send">
                                        <button id="submitBtn" onclick="knap.sendMessage('send-chat-box');return false;" class="btn btn-danger btn-rounded" type="submit"><?php echo app('translator')->getFromJson('core.send'); ?></button>
                                    </div>
                                </div>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
                <!-- .chat-right-panel -->
            </div>
            <!-- /.chat-row -->
            <!-- /.row -->

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <script src="<?php echo e(asset($global->theme_folder.'/js/chat.js')); ?>"></script>

    <!-- DataTables -->
    <script>
        $(function(){
            $('#userList').slimScroll({
                height: '250px'
            });
        });

        var dpButtonID = "";
        var dpName     = "";

        var dpClassID = '<?php echo e($dpData); ?>';

        if(dpClassID){ $('#dp_'+dpClassID).addClass('active');}

        //getting data
        knap.getChatData(dpButtonID , dpName);

    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>