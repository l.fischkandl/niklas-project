<!-- jQuery -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/sidebar-nav/dist/sidebar-nav.min.js')); ?>"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/js/jquery.slimscroll.js')); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo e(asset($global->theme_folder.'/js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset($global->theme_folder.'/js/custom.min.js')); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/plugins/bootstrap-fileinput/bootstrap-fileinput.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('common/js/laroute.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/knap.js')); ?>"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Logo path used in knap.js
    var logoPath = '<?php echo e($logoPath); ?>';

    // Avatar path used in knap.js
    var avatarPath = '<?php echo e($avatarPath); ?>';
</script>
<script src="<?php echo e(asset('common/js/laroute.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/knap.js')); ?>"></script>
<?php echo $__env->yieldContent('footerjs'); ?>

