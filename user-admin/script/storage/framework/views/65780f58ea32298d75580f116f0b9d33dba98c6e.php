<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo e($global->site_name); ?> | <?php echo e($pageTitle); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/bootstrap/css/bootstrap.min.css')); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/css/AdminLTE.css')); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/iCheck/square/blue.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/froiden-helper/helper.css')); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img src="<?php echo e(asset('/logo/'.$global->logo)); ?>" height="50px" alt="">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?php echo app('translator')->getFromJson('messages.signInMessage'); ?></p>

        <?php echo Form::open(['url' => '', 'method' => 'post','class'=>'login-form','id'=>'login-form']); ?>

            <div id="alert"></div>
            <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="<?php echo app('translator')->getFromJson('core.email'); ?>">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="<?php echo app('translator')->getFromJson('core.password'); ?>">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <?php if($global->remember_me == 1): ?>
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> <?php echo app('translator')->getFromJson('core.rememberMe'); ?>
                            </label>
                        </div>
                    <?php endif; ?>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="knap.login();return false;" ><?php echo app('translator')->getFromJson('core.signIn'); ?></button>
                </div>
                <!-- /.col -->
            </div>
        <?php echo Form::close(); ?>

        <div class = "social-auth-links text-center">
            <p>-OR-</p>
            <a href="<?php echo e(route('social.login',['facebook'])); ?>" class="btn btn-block btn-social btn-facebook btn-flat">
                <i class="fa fa-facebook"></i>
                <?php echo app('translator')->getFromJson('core.loginWithFacebook'); ?>
            </a>
            <a href="<?php echo e(route('social.login',['google'])); ?>" class="btn btn-block btn-social btn-google btn-flat">
                <i class="fa fa-google-plus"></i>
                <?php echo app('translator')->getFromJson('core.loginWithGoogle'); ?>
            </a>
            <a href="<?php echo e(route('social.login',['twitter'])); ?>" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-twitter"></i>
                <?php echo app('translator')->getFromJson('core.loginWithTwitter'); ?>
            </a>
        </div>

        <?php if($global->forget_password == 1): ?>
            <a href="<?php echo e(route('get-reset')); ?>" class="text-center"><?php echo app('translator')->getFromJson('core.forgetPassword'); ?></a><br>
        <?php endif; ?>
        <?php if($global->allow_register == 1): ?>
            <a href="<?php echo e(route('get-register')); ?>" class="text-center"><?php echo app('translator')->getFromJson('messages.signUpMessage'); ?></a>
        <?php endif; ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/jQuery/jQuery-2.2.0.min.js')); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo e(asset($global->theme_folder.'/bootstrap/js/bootstrap.min.js')); ?>"></script>
<!-- iCheck -->
<script src="<?php echo e(asset($global->theme_folder.'/plugins/iCheck/icheck.min.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/laroute.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/knap.js')); ?>"></script>
<!-- Laravel Javascript Validation -->
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
