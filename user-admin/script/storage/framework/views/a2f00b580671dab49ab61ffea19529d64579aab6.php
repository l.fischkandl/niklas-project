<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset($global->theme_folder.'/plugins/iCheck/minimal/_all.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-header'); ?>
    <section class="content-header">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard"></i> <?php echo app('translator')->getFromJson('menu.home'); ?></a></li>
            <li class=""> <?php echo app('translator')->getFromJson('menu.rolesPermissions'); ?></li>
            <li class="active"><?php echo app('translator')->getFromJson('menu.permissions'); ?></li>
        </ol>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <a href="javascript: ;" onclick="knap.addModal('permissions.create')" class="btn btn-success">
                        <span class="hidden-xs"> <?php echo app('translator')->getFromJson('core.btnAddPermission'); ?> </span><i class="fa fa-plus"></i>
                    </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body ">
                    <table id="permissions" class="table table-striped table-bordered dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th class="hidden"><?php echo app('translator')->getFromJson('core.id'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.name'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.displayName'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.description'); ?></th>
                            <th><?php echo app('translator')->getFromJson('core.actions'); ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('modals'); ?>
    <?php echo $__env->make($global->theme_folder.'.include.add-edit-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make($global->theme_folder.'.include.delete-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footerjs'); ?>
    <!-- DataTables -->
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset($global->theme_folder.'/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>
<script type="text/javascript">

var  table = $('#permissions').dataTable({
        "sScrollY": "100%",
    "scrollCollapse": true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo e(route('user.get-permissions')); ?>",
        "columns": [
            {data: 'id', name: 'id', orderable: false, searchable: false, visible:false},
            {data: 'name', name: 'name', orderable: true, searchable: true},
            {data: 'display_name', name: 'display_name', orderable: true, searchable: true},
            {data: 'description', name: 'description', orderable: true, searchable: true},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "oLanguage": {
            "sProcessing": '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($global->theme_folder.'.layouts.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>