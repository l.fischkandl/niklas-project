<!DOCTYPE html>
<html>
<head>
   <?php echo $__env->make($global->theme_folder.'.sections.meta-data', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   <?php echo $__env->make($global->theme_folder.'.sections.style', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</head>
<body class="hold-transition skin-<?php echo e($global->theme_color); ?> sidebar-mini">
<div class="wrapper">
    <?php echo $__env->make($global->theme_folder.'.sections.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php echo $__env->make($global->theme_folder.'.sections.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <?php echo $__env->yieldContent('page-header'); ?>

        <!-- Main content -->
            <section class="content">
                <?php echo $__env->yieldContent('content'); ?>
            </section>

            <!-- /.content -->
            <?php echo $__env->yieldContent('modals'); ?>
        </div>
        <!-- /.content-wrapper -->

     
    <?php echo $__env->make($global->theme_folder.'.sections.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<?php echo $__env->make($global->theme_folder.'.sections.footer-scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('scripts-footer'); ?>

</body>
</html>
