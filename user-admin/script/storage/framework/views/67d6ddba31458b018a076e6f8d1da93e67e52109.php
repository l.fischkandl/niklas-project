<!-- Left navbar-sidebar -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="nav-item <?php if(strpos(\Request::route()->getName(),'dashboard') !== false): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('dashboard.index')); ?>" class="waves-effect">
                    <i class="icon-home"></i>
                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.dashboard')); ?></span>
                    <!-- <span class="selected"></span> -->
                </a>
            </li>
            <?php if (\Entrust::can('view-users')) : ?>
                <li class="nav-item <?php if(strpos(\Request::route()->getName(),'users') !== false): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('users.index')); ?>" class="waves-effect">
                        <i class="icon-user"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.users')); ?></span>
                        <!-- <span class="selected"></span> -->
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can(['view-role', 'view-permission'])) : ?>
                <li class="nav-item <?php if(strpos(\Request::route()->getName(),'roles') or strpos(\Request::route()->getName(),'permissions')): ?> active <?php endif; ?>">
                    <a href="javascript:;" class="waves-effectnav-toggle">
                        <i class="icon-key"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.rolesPermissions')); ?></span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <?php if (\Entrust::can('view-role')) : ?>
                            <li class="nav-item <?php if(strpos(\Request::route()->getName(),'roles')): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('roles.index')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.roles')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('view-permission')) : ?>
                            <li class="nav-item <?php if(strpos(\Request::route()->getName(),'permissions')): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('permissions.index')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.permissions')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                    </ul>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can('view-activity-log')) : ?>
                <li class="nav-item <?php if(preg_match('/activity/',\Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('activity')); ?>" class="waves-effect">
                        <i class="icon-clock"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.activityLog')); ?></span>
                        <!-- <span class="selected"></span> -->
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <?php if (\Entrust::can('view-email-template')) : ?>
                <li class="nav-item <?php if(preg_match('/email-templates/',\Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('email-templates.index')); ?>" class="waves-effect">
                        <i class="icon-envelope"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.emailTemplates')); ?></span>
                        <!-- <span class="selected"></span> -->
                    </a>
                </li>
            <?php endif; // Entrust::can ?>
            <li class="nav-item <?php if(strpos(\Request::route()->getName(),'chat')): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('chat')); ?>" class="waves-effect">
                    <i class="fa fa-comments-o"></i>
                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.userChat')); ?></span>
                    <!-- <span class="selected"></span> -->
                </a>
            </li>
            <?php if($user->user_type=='admin'): ?>
                <li class="nav-item <?php if(preg_match('/session/',\Request::route()->getName())): ?> active <?php endif; ?>">
                    <a href="<?php echo e(route('sessions.index')); ?>" class="waves-effect">
                        <i class="fa fa-sign-out"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.session')); ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (\Entrust::can(['update-social-settings', 'update-general-settings', 'update-theme-settings', 'update-mail-settings', 'update-common-settings', 'manage-custom-fields' ])) : ?>
                <li class="nav-item <?php if(strpos(\Request::route()->getName(),'setting') || strpos(\Request::route()->getName(),'fields')): ?> active <?php endif; ?>">
                    <a href="javascript:;" class="waves-effectnav-toggle">
                        <i class="icon-settings"></i>
                        <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.settings')); ?></span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <?php if (\Entrust::can('update-social-settings')) : ?>
                            <li class="nav-item <?php if(preg_match('/social/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('social-settings')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.social')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-general-settings')) : ?>
                            <li class="nav-item <?php if(preg_match('/general/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('general-settings')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.general')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-theme-settings')) : ?>
                            <li class="nav-item <?php if(preg_match('/theme/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('theme-settings')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.theme')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('manage-custom-fields')) : ?>
                            <li class="nav-item <?php if(preg_match('/custom-fields/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('custom-fields.index')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.custom_fields')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-common-settings')) : ?>
                            <li class="nav-item <?php if(preg_match('/common-settings/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('common-settings')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.settings')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                        <?php if (\Entrust::can('update-mail-settings')) : ?>
                            <li class="nav-item <?php if(preg_match('/mail-settings/',\Request::route()->getName())): ?> active <?php endif; ?>">
                                <a href="<?php echo e(route('mail-settings')); ?>" class="waves-effect">
                                    <span class="hide-menu"><?php echo e(app('translator')->getFromJson('menu.mailSettings')); ?></span>
                                </a>
                            </li>
                        <?php endif; // Entrust::can ?>
                    </ul>
                </li>
            <?php endif; // Entrust::can ?>
        </ul>
    </div>
</div>
<!-- Left navbar-sidebar end -->