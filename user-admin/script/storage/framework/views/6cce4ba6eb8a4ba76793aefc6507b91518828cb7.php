<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?php echo e($global->site_name); ?> | <?php echo e($pageTitle); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/simple-line-icons/simple-line-icons.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap/css/bootstrap'.$rtl.'.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo e(asset($global->theme_folder.'/global/css/components-md'.$rtl.'.css')); ?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo e(asset($global->theme_folder.'/global/css/plugins-md'.$rtl.'.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?php echo e(asset($global->theme_folder.'/pages/css/login'.$rtl.'.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')); ?>"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="<?php echo e(asset($global->theme_folder.'/favicon.ico')); ?>" /> </head>
</head>
<!-- END HEAD -->

<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <img src="<?php echo e(asset('/logo/'.$global->logo)); ?>" height="40px" alt="">
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
<p id="alert"></p>
    <!-- BEGIN REGISTRATION FORM -->
    <?php echo Form::open(['url' => '', 'method' => 'POST','class'=>'login-form', 'id' => 'register-form']); ?>


        <h3 class="font-green"><?php echo app('translator')->getFromJson('core.signUp'); ?></h3>
        
        <p class="hint"> <?php echo app('translator')->getFromJson('messages.enterYourDetail'); ?>: </p>
        <div class="form-group">
            <label class="control-label">Name</label>
            <input class="form-control placeholder-no-fix" type="text" placeholder="Name" name="name" /> </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label"><?php echo app('translator')->getFromJson('core.email'); ?></label>
            <input class="form-control placeholder-no-fix" type="text" placeholder="<?php echo app('translator')->getFromJson('core.email'); ?>" name="email" /> </div>
        <div class="form-group">
            <label class="control-label"><?php echo app('translator')->getFromJson('core.password'); ?></label>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="<?php echo app('translator')->getFromJson('core.password'); ?>" name="password" /> </div>

        <div class="form-group">
            <label class="control-label"><?php echo app('translator')->getFromJson('core.rePassword'); ?></label>
            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo app('translator')->getFromJson('core.rePassword'); ?>" name="password_confirmation" />
        </div>

        <div class="form-group">
            <label class="control-label"><?php echo app('translator')->getFromJson('core.dob'); ?></label>
            <input type="date" class="form-control form-control-inline date-picker" size="16" name="dob"
                   id="datepicker" placeholder="<?php echo app('translator')->getFromJson('core.dob'); ?>" value="">
        </div>
        <div class="form-group">
        <label class="control-label"><?php echo app('translator')->getFromJson('core.gender'); ?></label>

        <div class="md-radio-list">
            <div class="md-radio">
                <input type="radio" name="gender" id="male" class="md-radio" value="male" checked>
                <label for="male">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> <?php echo app('translator')->getFromJson('core.male'); ?>
                </label>
            </div>
            <div class="md-radio">
                <input type="radio" name="gender" id="female" class="md-radio" value="female">
                <label for="female">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> <?php echo app('translator')->getFromJson('core.female'); ?>
                </label>
            </div>
        </div>
    </div>

        
        <?php if($global->custom_field_on_register == 1): ?>
            <?php $__currentLoopData = $fields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $field): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="form-group ">
                    <label  class="control-label"><?php echo e($field->label); ?></label>

                        <?php if( $field->type == 'text'): ?>
                            <input type="text" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">
                        <?php elseif($field->type == 'password'): ?>
                            <input type="password" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">
                        <?php elseif($field->type == 'number'): ?>
                            <input type="number" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" placeholder="<?php echo e($field->name); ?>" value="<?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?>">

                        <?php elseif($field->type == 'textarea'): ?>
                            <textarea name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" class="form-control" id="<?php echo e($field->name); ?>" cols="3" placeholder="<?php echo e(ucwords($field->name)); ?>"><?php echo e(isset($editUser->custom_fields_data['field_'.$field->id]) ? $editUser->custom_fields_data['field_'.$field->id] : ''); ?></textarea>

                        <?php elseif($field->type == 'radio'): ?>
                            <div class="md-radio-list">

                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="md-radio">
                                        <input type="radio" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]" id="optionsRadios<?php echo e($key.$field->id); ?>"
                                          <?php if($key==0): ?> checked <?php endif; ?>     class="md-radio" value="<?php echo e($value); ?>">
                                        <label for="optionsRadios<?php echo e($key.$field->id); ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <?php echo e($value); ?>

                                        </label>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php elseif($field->type == 'select'): ?>
                            <?php echo Form::select($field->name,
                                    $field->values,
                                     isset($editUser)?$editUser->custom_fields_data['field_'.$field->id]:'',['class' => 'form-control gender']); ?>


                        <?php elseif($field->type == 'checkbox'): ?>
                            <div class="mt-checkbox-inline">
                                <?php $__currentLoopData = $field->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>][]" type="checkbox" value="<?php echo e($key); ?>"> <?php echo e($value); ?>

                                        <span></span>
                                    </label>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        <?php elseif($field->type == 'date'): ?>
                            <input type="text" class="form-control form-control-inline date-picker" size="16" name="custom_fields_data[<?php echo e($field->name.'_'.$field->id); ?>]"
                                   id="datepicker" value="<?php echo e(isset($editUser->dob)?Carbon\Carbon::parse($editUser->dob)->format('Y-m-d'):Carbon\Carbon::now()->format('Y-m-d')); ?>">
                        <?php endif; ?>

                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

        <?php if($global->recaptcha == 1): ?>
            <div class="form-group">
                <label class="control-label"><?php echo app('translator')->getFromJson('core.captcha'); ?></label>
                <?php echo \Greggilbert\Recaptcha\Facades\Recaptcha::render(); ?>

            </div>
        <?php endif; ?>

        <div class="form-actions">
            <a href="<?php echo e(route('user.login')); ?>" id="register-back-btn" class="btn default"><?php echo app('translator')->getFromJson('core.back'); ?></a>
            <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right" onclick="knap.register('content');return false"><?php echo app('translator')->getFromJson('core.submit'); ?></button>
        </div>

<?php echo Form::close(); ?>

<!-- END LOGIN FORM -->
</div>
<!--[if lt IE 9]>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/respond.min.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/excanvas.min.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/ie8.fix.min.js')); ?>"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/jquery.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap/js/bootstrap.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/js.cookie.min.js')); ?>" type="text/javascript"></script>


<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo e(asset($global->theme_folder.'/global/scripts/app.min.js')); ?>" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/froiden-helper/helper.js')); ?>"></script>
<script src="<?php echo e(asset($global->theme_folder.'/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript" ></script>
<script src="<?php echo e(asset('common/js/laroute.js')); ?>"></script>
<script src="<?php echo e(asset('common/js/knap.js')); ?>"></script>
<script>

    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });


</script>

<!-- End Login Script-->
</body>

</html>