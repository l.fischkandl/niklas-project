<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-sunglo">
            <i class="icon-<?php echo e(isset($iconEdit) ? $iconEdit : $icon); ?> font-red-sunglo"></i>
            <span class="caption-subject bold uppercase">
         <?php if(isset($role->id)): ?> <?php echo e(app('translator')->getFromJson('menu.editRole')); ?><?php else: ?> <?php echo e(app('translator')->getFromJson('menu.addRole')); ?> <?php endif; ?>
            </span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>

    <div class="portlet-body form">


        <?php echo Form::open(['url' => '' ,'method' => 'post', 'id' => 'add-edit-form','class'=>'form-horizontal']); ?>

        <?php if(isset($role->id)): ?> <input type="hidden" name="_method" value="PUT"> <?php endif; ?>
            <div class="box-body form">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-sm-2 control-label" for="name"><?php echo e(app('translator')->getFromJson('core.name')); ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="name" class="form-control"  value="<?php echo e(isset($role->name) ? $role->name : old('name')); ?>" placeholder="<?php echo e(app('translator')->getFromJson('core.enterRoleName')); ?>">
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-sm-2 control-label" for="display_name"><?php echo e(app('translator')->getFromJson('core.displayName')); ?></label>
                        <div class="col-sm-10">
                            <input type="text" id="display_name" class="form-control"  value="<?php echo e(isset($role->display_name) ? $role->display_name : old('display_name')); ?>" name="display_name" placeholder="<?php echo e(app('translator')->getFromJson('core.enterDisplayName')); ?>">
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-sm-2 control-label" for="description"><?php echo e(app('translator')->getFromJson('core.description')); ?></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="description" id="description"><?php echo e(isset($role->description) ? $role->description : old('description')); ?></textarea>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group form-md-checkboxes">
                        <label class="col-md-2 control-label"><?php echo e(app('translator')->getFromJson('core.selectPermission')); ?></label>
                        <div class="col-md-10">
                            <div class="md-checkbox-inline">
                                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="md-checkbox">
                                        <input type="checkbox" id="id<?php echo e($permission->id); ?>" value="<?php echo e($permission->id); ?>" name="permission[<?php echo e($permission->id); ?>]" class="md-check" <?php if(isset($perms) && in_array($permission->id, $perms)): ?> checked <?php endif; ?> >
                                        <label for="id<?php echo e($permission->id); ?>">
                                            <span class="inc"></span>
                                            <span class="check"></span>
                                            <span class="box"></span> <?php echo e($permission->display_name); ?> </label>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
    <button class="btn  dark " data-dismiss="modal" aria-hidden="true"><?php echo e(app('translator')->getFromJson('core.close')); ?></button>
    <button id="save" type="submit" class="btn  green" onclick="knap.addUpdate('roles', '<?php echo e(isset($role->id) ? $role->id : ''); ?>');return false"><?php echo e(app('translator')->getFromJson('core.submit')); ?></button>
</div>
        <?php echo e(Form::close()); ?>

    </div>
</div>

